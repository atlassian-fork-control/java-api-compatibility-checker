# Overview #
This is an utility for checking the compatibility of API versions. Its structured to run in CI, for running on local machine slight changes in python script may be required.

It uses https://github.com/lvc/japi-compliance-checker/

# How to use it #
Refer sample configuration files under *conf* folder

```
#!xml

<artifacts>
    <artifact name="atlassian-rest-module">
        <groupId>maven-artifact-group-id</groupId>
        <artifactId>maven-artifact-id</artifactId>
        <reference-version>api-benchmark-version</reference-version>
        <!--
        <exclude>package name or fully qualified classname with .class extension</exclude>
                          OR
        <include>package name or fully qualified classname with .class extension</include>
        -->
	</artifact>
</artifacts>

```
Example # 1
```
#!xml

<artifacts>
    <artifact name="atlassian-plugins-api">
        <groupId>com.atlassian.plugins</groupId>
        <artifactId>atlassian-plugins-api</artifactId>
        <reference-version>4.0.0</reference-version>
        <!-- Excludes all classes in the org.apache.commons package and com.google.String class -->
        <exclude>org.apache.commons,com.google.String.class</exclude>
    </artifact>
</artifacts>
```
Example # 2
```
#!xml

<artifacts>
    <artifact name="atlassian-plugins-api">
        <groupId>com.atlassian.plugins</groupId>
        <artifactId>atlassian-plugins-api</artifactId>
        <reference-version>4.0.0</reference-version>
        <!-- ONLY include all classes in the com.atlassian package and com.google.String class -->
        <include>com.atlassian,com.google.String.class</include>
    </artifact>
</artifacts>
```
# Note #
1. It wont support any wildcard characters like * (e.g PluginAccessor*)
2. If you are adding include tag, scanner will check ONLY the classes matching that pattern.
3. You can add more than one package/class separated by comma.
4. If the artifact name contains word 'api', it will automatically use Java API compatibility rules, else normal rules would be used.

# Referance #
[API Compatibility Bamboo Plan for Atlassian Plugins](https://ecosystem-bamboo.internal.atlassian.com/browse/NRAM-ACP/branches)
Check the reports under build **artifact** tab.